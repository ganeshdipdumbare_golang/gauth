package main

import (
	"fmt"
	"net/http"

	"golang.org/x/oauth2"
	"golang.org/x/oauth2/google"
)

var (
	gconfig = &oauth2.Config{
		RedirectURL:  "http://localhost:8080/callback",
		ClientID:     "",
		ClientSecret: "",
		Scopes:       []string{"https://www.googleapis.com/auth/userinfo.email"},
		Endpoint:     google.Endpoint,
	}
	randomState = "myappuser"
)

func main() {
	http.HandleFunc("/", HandleHome)
	http.HandleFunc("/login", HandleLogin)
	http.HandleFunc("/callback", HandleCallback)
	err := http.ListenAndServe(":8080", nil)
	if err != nil {
		fmt.Println("Unable to start the server", err)
	}
}

func HandleHome(w http.ResponseWriter, r *http.Request) {
	var html = `<html><body><a href="/login">Google Login</a></body></html>`
	fmt.Fprintf(w, html)
}

func HandleLogin(w http.ResponseWriter, r *http.Request) {
	url := gconfig.AuthCodeURL(randomState)
	http.Redirect(w, r, url, http.StatusTemporaryRedirect)

}

func HandleCallback(w http.ResponseWriter, r *http.Request) {
	token, err := gconfig.Exchange(oauth2.NoContext, r.FormValue("code"))
	fmt.Println("Error", err)
	fmt.Fprintf(w, token.AccessToken)
}
